# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

1. Create virtual env
python3 -m venv env
source env/bin/activate

2. Install dependencies
pip3 install django social-auth-app-django

3. Migration
python3 manage.py migrate

4. Run
python3 manage.py runserver